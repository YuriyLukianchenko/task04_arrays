package com.lukianchenko.arraysABCD.model;

public class ArrayManager {
    private int id;

    private Object [] array1;
    private Object [] array2;

    public ArrayManager() {
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void inittiate2Arrays(){
        array1 = new Object[5];
        array1[0] = "string";
        array1[1] = 2;
        array1[2] = true;
        array1[3] = 4.5;
        array1[4] = new ArrayManager();

        array2 = new Object[5];
        array2[0] =false ;
        array2[1] = 4.7;
        array2[2] = "string";
        array2[3] = 2;
        array2[4] = new ArrayManager();
    }


    @Override
    public String toString(){
        return "ArrayManager Id = " + Integer.toString(id);
    }
    @Override
    public int hashCode(){
        return this.getId();
    }
    @Override
    public boolean equals(Object obj){
        if (obj instanceof ArrayManager){
            ArrayManager array = (ArrayManager)obj;
            if (array.getId() == this.getId()){
                return true;
            }
        }
        return false;
    }
}
